// Selection de la piece
const uiTableau = document.querySelector('.tableau-content');

// Fonction shogi
function shogi() {

    // Liste de pions
    let pions = [
        {nom: '1_B', image: './img/1_B.png'},    // 0
        {nom: '1_B+', image: './img/1_B+.png'},  // 1
        {nom: '1_G', image: './img/1_G.png'},    // 2
        {nom: '1_K', image: './img/1_K.png'},    // 3
        {nom: '1_L', image: './img/1_L.png'},    // 4
        {nom: '1_L+', image: './img/1_L+.png'},  // 5
        {nom: '1_N', image: './img/1_N.png'},    // 6
        {nom: '1_N+', image: './img/1_N+.png'},  // 7
        {nom: '1_P', image: './img/1_P.png'},    // 8
        {nom: '1_P+', image: './img/1_P+.png'},  // 9
        {nom: '1_R', image: './img/1_R.png'},    // 10
        {nom: '1_R+', image: './img/1_R+.png'},  // 11
        {nom: '1_S', image: './img/1_S.png'},    // 12
        {nom: '1_S+', image: './img/1_S+.png'},  // 13
        {nom: '2_B', image: './img/2_B.png'},    // 14
        {nom: '2_B+', image: './img/2_B+.png'},  // 15
        {nom: '2_G', image: './img/2_G.png'},    // 16
        {nom: '2_K', image: './img/2_K.png'},    // 17
        {nom: '2_L', image: './img/2_L.png'},    // 18
        {nom: '2_L+', image: './img/2_L+.png'},  // 19
        {nom: '2_N', image: './img/2_N.png'},    // 20
        {nom: '2_N+', image: './img/2_N+.png'},  // 21
        {nom: '2_P', image: './img/2_P.png'},    // 22
        {nom: '2_P+', image: './img/2_P+.png'},  // 23
        {nom: '2_R', image: './img/2_R.png'},    // 24
        {nom: '2_R+', image: './img/2_R+.png'},  // 25
        {nom: '2_S', image: './img/2_S.png'},    // 26
        {nom: '2_S+', image: './img/2_S+.png'},  // 27
    ];

    // Liste du tableau
    let tableau = [
        [{type: "L2", joueur: 2, dp: 0}, {type: "N2", joueur: 2, dp: 0}, {type: "S2", joueur: 2, dp: 0}, {
            type: "G2",
            joueur: 2,
            dp: 0
        },
            {type: "K", joueur: 2, dp: 0}, {type: "G1", joueur: 2, dp: 0}, {type: "S1", joueur: 2, dp: 0}, {
            type: "N1",
            joueur: 2,
            dp: 0
        },
            {type: "L1", joueur: 2, dp: 0}],
        [{type: "0", joueur: 0, dp: 0}, {type: "R", joueur: 2, dp: 0}, {type: "0", joueur: 0, dp: 0}, {
            type: "0",
            joueur: 0,
            dp: 0
        },
            {type: "0", joueur: 0, dp: 0}, {type: "0", joueur: 0, dp: 0}, {type: "0", joueur: 0, dp: 0}, {
            type: "B",
            joueur: 2,
            dp: 0
        },
            {type: "0", joueur: 0, dp: 0}],
        [{type: "P9", joueur: 2, dp: 0}, {type: "P8", joueur: 2, dp: 0}, {type: "P7", joueur: 2, dp: 0}, {
            type: "P6",
            joueur: 2,
            dp: 0
        },
            {type: "P5", joueur: 2, dp: 0}, {type: "P4", joueur: 2, dp: 0}, {type: "P3", joueur: 2, dp: 0}, {
            type: "P2",
            joueur: 2,
            dp: 0
        },
            {type: "P1", joueur: 2, dp: 0}],
        [{type: "0", joueur: 0, dp: 0}, {type: "0", joueur: 0, dp: 0}, {type: "0", joueur: 0, dp: 0}, {
            type: "0",
            joueur: 0,
            dp: 0
        },
            {type: "0", joueur: 0, dp: 0}, {type: "0", joueur: 0, dp: 0}, {type: "0", joueur: 0, dp: 0}, {
            type: "0",
            joueur: 0,
            dp: 0
        },
            {type: "0", joueur: 0, dp: 0}],
        [{type: "0", joueur: 0, dp: 0}, {type: "0", joueur: 0, dp: 0}, {type: "0", joueur: 0, dp: 0}, {
            type: "0",
            joueur: 0,
            dp: 0
        },
            {type: "0", joueur: 0, dp: 0}, {type: "0", joueur: 0, dp: 0}, {type: "0", joueur: 0, dp: 0}, {
            type: "0",
            joueur: 0,
            dp: 0
        },
            {type: "0", joueur: 0, dp: 0}],
        [{type: "0", joueur: 0, dp: 0}, {type: "0", joueur: 0, dp: 0}, {type: "0", joueur: 0, dp: 0}, {
            type: "0",
            joueur: 0,
            dp: 0
        },
            {type: "0", joueur: 0, dp: 0}, {type: "0", joueur: 0, dp: 0}, {type: "0", joueur: 0, dp: 0}, {
            type: "0",
            joueur: 0,
            dp: 0
        },
            {type: "0", joueur: 0, dp: 0}],
        [{type: "P1", joueur: 1, dp: 0}, {type: "P2", joueur: 1, dp: 0}, {type: "P3", joueur: 1, dp: 0}, {
            type: "P4",
            joueur: 1,
            dp: 0
        },
            {type: "P5", joueur: 1, dp: 0}, {type: "P6", joueur: 1, dp: 0}, {type: "P7", joueur: 1, dp: 0}, {
            type: "P8",
            joueur: 1,
            dp: 0
        },
            {type: "P9", joueur: 1, dp: 0}],
        [{type: "0", joueur: 0, dp: 0}, {type: "B", joueur: 1, dp: 0}, {type: "0", joueur: 0, dp: 0}, {
            type: "0",
            joueur: 0,
            dp: 0
        },
            {type: "0", joueur: 0, dp: 0}, {type: "0", joueur: 0, dp: 0}, {type: "0", joueur: 0, dp: 0}, {
            type: "R",
            joueur: 1,
            dp: 0
        },
            {type: "0", joueur: 0, dp: 0}],
        [{type: "L1", joueur: 1, dp: 0}, {type: "N1", joueur: 1, dp: 0}, {type: "S1", joueur: 1, dp: 0}, {
            type: "G1",
            joueur: 1,
            dp: 0
        },
            {type: "K", joueur: 1, dp: 0}, {type: "G2", joueur: 1, dp: 0}, {type: "S2", joueur: 1, dp: 0}, {
            type: "N2",
            joueur: 1,
            dp: 0
        },
            {type: "L2", joueur: 1, dp: 0}]
    ];

    // Instanciation des letiables
    let position_x = 0;
    let position_y = 0;
    let num_pion;
    let num_joueur;
    let num_children;
    let num_deplacement;
    let num_initial;
    let num_dp;
    let nom_pion;
    let pion_devore;
    let pion_devore_joueur;
    let id_joueur = 1;
    let old_x;
    let old_y;
    let old2_x;
    let old2_y;

    // Tour du joueur 1
    let tourJoueur1 = true;

    // Tour du joueur 2
    let tourJoueur2 = false;

    // Si la partie est terminee
    let partieTerminee = false;

    // Fonction d'ajout d'image sur la grille
    let addImage = (cell, numerocell, n) => {
        // Create new image with the X or the O
        let img = new Image();

        // Format de l'image
        img.width = 50;
        img.height = 50;

        // Ajouter l'image si la case est vide
        if (uiTableau.children[numerocell].innerHTML === '') {
            img.src = pions[n].image;
            cell.appendChild(img);
            return img;
        }
    };

    // Fonction de suppression d'image sur la grille
    let removeImage = (cell, numerocell) => {
        uiTableau.children[numerocell].innerHTML = '';
    };

    // Fonction de promotion de pion
    function PromotionPion(x, y, pion, joueur) {

        // Obtenir la valeur de la case de l'interface
        num_children = (y * 9 + x);

        // Lorsque un pion du joueur 1 se retrouve dans les 3 dernieres rangees
        if (joueur === 1 && y <= 2 && y >= 0) {
            // Promotion du pion L
            if (pion === 'L1' || pion === 'L2') {
                removeImage(uiTableau.children[num_children], num_children);
                addImage(uiTableau.children[num_children], num_children, 5);
                tableau[y][x] = {type: 'L+', joueur: joueur, dp: 0};
            }
            // Promotion du pion N
            else if (pion === 'N1' || pion === 'N2') {
                removeImage(uiTableau.children[num_children], num_children);
                addImage(uiTableau.children[num_children], num_children, 7);
                tableau[y][x] = {type: 'N+', joueur: joueur, dp: 0};
            }
            // Promotion du pion S
            else if (pion === 'S1' || pion === 'S2') {
                removeImage(uiTableau.children[num_children], num_children);
                addImage(uiTableau.children[num_children], num_children, 13);
                tableau[y][x] = {type: 'S+', joueur: joueur, dp: 0};
            }
            // Promotion du pion P
            else if (pion === 'P1' || pion === 'P2' || pion === 'P3' || pion === 'P4' || pion === 'P5' ||
                pion === 'P6' || pion === 'P7' || pion === 'P8' || pion === 'P9') {
                removeImage(uiTableau.children[num_children], num_children);
                addImage(uiTableau.children[num_children], num_children, 9);
                tableau[y][x] = {type: 'P+', joueur: joueur, dp: 0};
            }
            // Promotion du pion B
            else if (pion === 'B') {
                removeImage(uiTableau.children[num_children], num_children);
                addImage(uiTableau.children[num_children], num_children, 1);
                tableau[y][x] = {type: 'B+', joueur: joueur, dp: 0};
            }
            // Promotion du pion R
            else if (pion === 'R') {
                removeImage(uiTableau.children[num_children], num_children);
                addImage(uiTableau.children[num_children], num_children, 11);
                tableau[y][x] = {type: 'R+', joueur: joueur, dp: 0};
            }
        }

        // Lorsque un pion du joueur 1 se retrouve dans les 3 dernieres rangees
        else if (joueur === 2 && y <= 8 && y >= 6) {
            // Promotion du pion L
            if (pion === 'L1' || pion === 'L2') {
                removeImage(uiTableau.children[num_children], num_children);
                addImage(uiTableau.children[num_children], num_children, 19);
                tableau[y][x] = {type: 'L+', joueur: joueur, dp: 0};
            }
            // Promotion du pion N
            else if (pion === 'N1' || pion === 'N2') {
                removeImage(uiTableau.children[num_children], num_children);
                addImage(uiTableau.children[num_children], num_children, 21);
                tableau[y][x] = {type: 'N+', joueur: joueur, dp: 0};
            }
            // Promotion du pion S
            else if (pion === 'S1' || pion === 'S2') {
                removeImage(uiTableau.children[num_children], num_children);
                addImage(uiTableau.children[num_children], num_children, 27);
                tableau[y][x] = {type: 'S+', joueur: joueur, dp: 0};
            }
            // Promotion du pion P
            else if (pion === 'P1' || pion === 'P2' || pion === 'P3' || pion === 'P4' || pion === 'P5' ||
                pion === 'P6' || pion === 'P7' || pion === 'P8' || pion === 'P9') {
                removeImage(uiTableau.children[num_children], num_children);
                addImage(uiTableau.children[num_children], num_children, 23);
                tableau[y][x] = {type: 'P+', joueur: joueur, dp: 0};
            }
            // Promotion du pion B
            else if (pion === 'B') {
                removeImage(uiTableau.children[num_children], num_children);
                addImage(uiTableau.children[num_children], num_children, 15);
                tableau[y][x] = {type: 'B+', joueur: joueur, dp: 0};
            }
            // Promotion du pion R
            else if (pion === 'R') {
                removeImage(uiTableau.children[num_children], num_children);
                addImage(uiTableau.children[num_children], num_children, 25);
                tableau[y][x] = {type: 'R+', joueur: joueur, dp: 0};
            }
        }
    }

    // Fonction qui gere les deplacements selon les pions
    function DetectionDeplacement(x, y, pion, joueur) {

        // Obtenir la valeur de la case de l'interface
        num_children = (y * 9 + x);

        // Si le pion est L du joueur 1
        if ((pion === 'L1' || pion === 'L2') && joueur === 1 && tourJoueur1 && tableau[y][x].dp !== 2) {
            alert("Joueur 1, SELECTION " + pion + ".");
            old_x = x;
            old_y = y;

            // Toutes les case d'une direction
            for (let i = num_children - 9; i >= 0; i = i - 9) {
                y--;
                try {
                    if ((tableau[y][x].type === '0' || tableau[y][x].joueur === 2) && y <= 8 && y >= 0 && x <= 8 && x >= 0) {
                        if (tableau[y][x].type === '0') {
                            tableau[y][x].joueur = 1;
                            tableau[y][x].dp = 1;
                        } else {
                            tableau[y][x].dp = 2;
                        }
                        nom_pion = pion;
                        num_deplacement = 4;
                        num_initial = num_children;

                    } else {
                        break;
                    }
                } catch (err) {
                    num_deplacement = 4;
                }
            }
        }

        // Si le pion est L du joueur 2
        else if ((pion === 'L1' || pion === 'L2') && joueur === 2 && tourJoueur2 && tableau[y][x].dp !== 2) {
            alert("Joueur 2, SELECTION " + pion + ".");
            old_x = x;
            old_y = y;

            // Toutes les cases d'une direction
            for (let i = num_children + 9; i <= 80; i = i + 9) {
                y++;
                try {
                    if ((tableau[y][x].type === '0' || tableau[y][x].joueur === 1) && y <= 8 && y >= 0 && x <= 8 && x >= 0) {
                        if (tableau[y][x].type === '0') {
                            tableau[y][x].joueur = 2;
                            tableau[y][x].dp = 1;
                        } else {
                            tableau[y][x].dp = 2;
                        }
                        nom_pion = pion;
                        num_deplacement = 18;
                        num_initial = num_children;
                    } else {
                        break;
                    }
                } catch (err) {
                    num_deplacement = 18;
                }
            }
        }

        // Si le pion est N du joueur 1
        else if ((pion === 'N1' || pion === 'N2') && joueur === 1 && tourJoueur1 && tableau[y][x].dp !== 2) {
            alert("Joueur 1, SELECTION " + pion + ".");
            old_x = x;
            old_y = y;

            // Direction 1
            try {
                if ((tableau[y - 2][x - 1].type === '0' || tableau[y - 1][x - 1].joueur === 2) && y - 2 <= 8 && y - 2 >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                    if (tableau[y - 2][x - 1].type === '0') {
                        tableau[y - 2][x - 1].joueur = 1;
                        tableau[y - 2][x - 1].dp = 1;
                    } else {
                        tableau[y - 2][x - 1].dp = 2;
                    }
                    nom_pion = pion;
                    num_deplacement = 6;
                    num_initial = num_children;
                }
            } catch (err) {
                num_deplacement = 6;
            }

            // Direction 2
            try {
                if ((tableau[y - 2][x + 1].type === '0' || tableau[y - 2][x + 1].joueur === 2) && y - 2 <= 8 && y - 2 >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                    if (tableau[y - 2][x + 1].type === '0') {
                        tableau[y - 2][x + 1].joueur = 1;
                        tableau[y - 2][x + 1].dp = 1;
                    } else {
                        tableau[y - 2][x + 1].dp = 2;
                    }
                    nom_pion = pion;
                    num_deplacement = 6;
                    num_initial = num_children;
                }
            } catch (err) {
                num_deplacement = 6;
            }
        }

        // Si le pion est N du joueur 2
        else if ((pion === 'N1' || pion === 'N2') && joueur === 2 && tourJoueur2 && tableau[y][x].dp !== 2) {
            alert("Joueur 2, SELECTION " + pion + ".");
            old_x = x;
            old_y = y;

            // Direction 1
            try {
                if ((tableau[y + 2][x - 1].type === '0' || tableau[y + 2][x - 1].joueur === 1) && y + 2 <= 8 && y + 2 >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                    if (tableau[y + 2][x - 1].type === '0') {
                        tableau[y + 2][x - 1].joueur = 2;
                        tableau[y + 2][x - 1].dp = 1;
                    } else {
                        tableau[y + 2][x - 1].dp = 2;
                    }
                    nom_pion = pion;
                    num_deplacement = 20;
                    num_initial = num_children;
                }
            } catch (err) {
                num_deplacement = 20;
            }

            // Direction 2
            try {
                if ((tableau[y + 2][x + 1].type === '0' || tableau[y + 2][x + 1].joueur === 1) && y + 2 <= 8 && y + 2 >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                    if (tableau[y + 2][x + 1].type === '0') {
                        tableau[y + 2][x + 1].joueur = 2;
                        tableau[y + 2][x + 1].dp = 1;
                    } else {
                        tableau[y + 2][x + 1].dp = 2;
                    }
                    nom_pion = pion;
                    num_deplacement = 20;
                    num_initial = num_children;
                }
            } catch (err) {
                num_deplacement = 20;
            }
        }

        // Si le pion est S du joueur 1
        else if ((pion === 'S1' || pion === 'S2') && joueur === 1 && tourJoueur1 && tableau[y][x].dp !== 2) {
            alert("Joueur 1, SELECTION " + pion + ".");
            old_x = x;
            old_y = y;

            // Direction 1
            try {
                if ((tableau[y - 1][x - 1].type === '0' || tableau[y - 1][x - 1].joueur === 2) && y - 1 <= 8 && y - 1 >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                    if (tableau[y - 1][x - 1].type === '0') {
                        tableau[y - 1][x - 1].joueur = 1;
                        tableau[y - 1][x - 1].dp = 1;
                    } else {
                        tableau[y - 1][x - 1].dp = 2;
                    }
                    nom_pion = pion;
                    num_deplacement = 12;
                    num_initial = num_children;
                }
            } catch (err) {
                num_deplacement = 12;
            }

            // Direction 2
            try {
                if ((tableau[y - 1][x].type === '0' || tableau[y - 1][x].joueur === 2) && y - 1 <= 8 && y - 1 >= 0 && x <= 8 && x >= 0) {
                    if (tableau[y - 1][x].type === '0') {
                        tableau[y - 1][x].joueur = 1;
                        tableau[y - 1][x].dp = 1;
                    } else {
                        tableau[y - 1][x].dp = 2;
                    }
                    nom_pion = pion;
                    num_deplacement = 12;
                    num_initial = num_children;
                }
            } catch (err) {
                num_deplacement = 12;
            }

            // Direction 3
            try {
                if ((tableau[y - 1][x + 1].type === '0' || tableau[y][x].joueur === 2) && y - 1 <= 8 && y - 1 >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                    if (tableau[y - 1][x + 1].type === '0') {
                        tableau[y - 1][x + 1].joueur = 1;
                        tableau[y - 1][x + 1].dp = 1;
                    } else {
                        tableau[y - 1][x + 1].dp = 2;
                    }
                    nom_pion = pion;
                    num_deplacement = 12;
                    num_initial = num_children;
                }
            } catch (err) {
                num_deplacement = 12;
            }

            // Direction 4
            try {
                if ((tableau[y + 1][x - 1].type === '0' || tableau[y + 1][x - 1].joueur === 2) && y + 1 <= 8 && y + 1 >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                    if (tableau[y + 1][x - 1].type === '0') {
                        tableau[y + 1][x - 1].joueur = 1;
                        tableau[y + 1][x - 1].dp = 1;
                    } else {
                        tableau[y + 1][x - 1].dp = 2;
                    }
                    nom_pion = pion;
                    num_deplacement = 12;
                    num_initial = num_children;
                }
            } catch (err) {
                num_deplacement = 12;
            }

            // Direction 5
            try {
                if ((tableau[y + 1][x + 1].type === '0' || tableau[y + 1][x + 1].joueur === 2) && y + 1 <= 8 && y + 1 >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                    if (tableau[y + 1][x + 1].type === '0') {
                        tableau[y + 1][x + 1].joueur = 1;
                        tableau[y + 1][x + 1].dp = 1;
                    } else {
                        tableau[y + 1][x + 1].dp = 2;
                    }
                    nom_pion = pion;
                    num_deplacement = 12;
                    num_initial = num_children;

                }
            } catch (err) {
                num_deplacement = 12;
            }
        }

        // Si le pion est S du joueur 2
        else if ((pion === 'S1' || pion === 'S2') && joueur === 2 && tourJoueur2 && tableau[y][x].dp !== 2) {
            alert("Joueur 2, SELECTION " + pion + ".");
            old_x = x;
            old_y = y;

            // Direction 1
            try {
                if ((tableau[y + 1][x + 1].type === '0' || tableau[y + 1][x + 1].joueur === 1) && y + 1 <= 8 && y + 1 >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                    nom_pion = pion;
                    num_deplacement = 26;
                    num_initial = num_children;
                    if (tableau[y + 1][x + 1].type === '0') {
                        tableau[y + 1][x + 1].joueur = 2;
                        tableau[y + 1][x + 1].dp = 1;
                    } else {
                        tableau[y + 1][x + 1].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 26;
            }

            // Direction 2
            try {
                if ((tableau[y + 1][x].type === '0' || tableau[y + 1][x].joueur === 1) && y + 1 <= 8 && y + 1 >= 0 && x <= 8 && x >= 0) {
                    nom_pion = pion;
                    num_deplacement = 26;
                    num_initial = num_children;
                    if (tableau[y + 1][x].type === '0') {
                        tableau[y + 1][x].joueur = 2;
                        tableau[y + 1][x].dp = 1;
                    } else {
                        tableau[y + 1][x].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 26;
            }

            // Direction 3
            try {
                if ((tableau[y + 1][x - 1].type === '0' || tableau[y + 1][x - 1].joueur === 1) && y + 1 <= 8 && y + 1 >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                    nom_pion = pion;
                    num_deplacement = 26;
                    num_initial = num_children;
                    if (tableau[y + 1][x - 1].type === '0') {
                        tableau[y + 1][x - 1].joueur = 2;
                        tableau[y + 1][x - 1].dp = 1;
                    } else {
                        tableau[y + 1][x - 1].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 26;
            }

            // Direction 4
            try {
                if ((tableau[y - 1][x + 1].type === '0' || tableau[y - 1][x + 1].joueur === 1) && y - 1 <= 8 && y - 1 >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                    nom_pion = pion;
                    num_deplacement = 26;
                    num_initial = num_children;
                    if (tableau[y - 1][x + 1].type === '0') {
                        tableau[y - 1][x + 1].joueur = 2;
                        tableau[y - 1][x + 1].dp = 1;
                    } else {
                        tableau[y - 1][x + 1].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 26;
            }

            // Direction 5
            try {
                if ((tableau[y - 1][x - 1].type === '0' || tableau[y - 1][x - 1].joueur === 1) && y - 1 <= 8 && y - 1 >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                    nom_pion = pion;
                    num_deplacement = 26;
                    num_initial = num_children;
                    if (tableau[y - 1][x - 1].type === '0') {
                        tableau[y - 1][x - 1].joueur = 2;
                        tableau[y - 1][x - 1].dp = 1;
                    } else {
                        tableau[y - 1][x - 1].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 26;
            }
        }

        // Si le pion est G, L+, N+, S+ ou P+ du joueur 1
        else if ((pion === 'G1' || pion === 'G2' || pion === 'L+' || pion === 'N+' || pion === 'S+' || pion === 'P+') && joueur === 1 && tourJoueur1 && tableau[y][x].dp !== 2) {
            alert("Joueur 1, SELECTION " + pion + ".");
            old_x = x;
            old_y = y;

            // Direction 1
            try {
                if ((tableau[y - 1][x - 1].type === '0' || tableau[y - 1][x - 1].joueur === 2) && y - 1 <= 8 && y - 1 >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                    nom_pion = pion;
                    if (pion === 'L+') {
                        num_deplacement = 5;
                    } else if (pion === 'N+') {
                        num_deplacement = 7;
                    } else if (pion === 'S+') {
                        num_deplacement = 13;
                    } else if (pion === 'P+') {
                        num_deplacement = 9;
                    } else {
                        num_deplacement = 2;
                    }
                    num_initial = num_children;
                    if (tableau[y - 1][x - 1].type === '0') {
                        tableau[y - 1][x - 1].joueur = 1;
                        tableau[y - 1][x - 1].dp = 1;
                    } else {
                        tableau[y - 1][x - 1].dp = 2;
                    }
                }
            } catch (err) {
                if (pion === 'L+') {
                    num_deplacement = 5;
                } else if (pion === 'N+') {
                    num_deplacement = 7;
                } else if (pion === 'S+') {
                    num_deplacement = 13;
                } else if (pion === 'P+') {
                    num_deplacement = 9;
                } else {
                    num_deplacement = 2;
                }
            }

            // Direction 2
            try {
                if ((tableau[y - 1][x].type === '0' || tableau[y - 1][x].joueur === 2) && y - 1 <= 8 && y - 1 >= 0 && x <= 8 && x >= 0) {
                    nom_pion = pion;
                    if (pion === 'L+') {
                        num_deplacement = 5;
                    } else if (pion === 'N+') {
                        num_deplacement = 7;
                    } else if (pion === 'S+') {
                        num_deplacement = 13;
                    } else if (pion === 'P+') {
                        num_deplacement = 9;
                    } else {
                        num_deplacement = 2;
                    }
                    num_initial = num_children;
                    if (tableau[y - 1][x].type === '0') {
                        tableau[y - 1][x].joueur = 1;
                        tableau[y - 1][x].dp = 1;
                    } else {
                        tableau[y - 1][x].dp = 2;
                    }
                }
            } catch (err) {
                if (pion === 'L+') {
                    num_deplacement = 5;
                } else if (pion === 'N+') {
                    num_deplacement = 7;
                } else if (pion === 'S+') {
                    num_deplacement = 13;
                } else if (pion === 'P+') {
                    num_deplacement = 9;
                } else {
                    num_deplacement = 2;
                }
            }

            // Direction 3
            try {
                if ((tableau[y - 1][x + 1].type === '0' || tableau[y - 1][x + 1].joueur === 2) && y - 1 <= 8 && y - 1 >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                    nom_pion = pion;
                    if (pion === 'L+') {
                        num_deplacement = 5;
                    } else if (pion === 'N+') {
                        num_deplacement = 7;
                    } else if (pion === 'S+') {
                        num_deplacement = 13;
                    } else if (pion === 'P+') {
                        num_deplacement = 9;
                    } else {
                        num_deplacement = 2;
                    }
                    num_initial = num_children;
                    if (tableau[y - 1][x + 1].type === '0') {
                        tableau[y - 1][x + 1].joueur = 1;
                        tableau[y - 1][x + 1].dp = 1;
                    } else {
                        tableau[y - 1][x + 1].dp = 2;
                    }
                }
            } catch (err) {
                if (pion === 'L+') {
                    num_deplacement = 5;
                } else if (pion === 'N+') {
                    num_deplacement = 7;
                } else if (pion === 'S+') {
                    num_deplacement = 13;
                } else if (pion === 'P+') {
                    num_deplacement = 9;
                } else {
                    num_deplacement = 2;
                }
            }

            // Direction 4
            try {
                if ((tableau[y][x - 1].type === '0' || tableau[y][x - 1].joueur === 2) && y <= 8 && y >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                    nom_pion = pion;
                    if (pion === 'L+') {
                        num_deplacement = 5;
                    } else if (pion === 'N+') {
                        num_deplacement = 7;
                    } else if (pion === 'S+') {
                        num_deplacement = 13;
                    } else if (pion === 'P+') {
                        num_deplacement = 9;
                    } else {
                        num_deplacement = 2;
                    }
                    num_initial = num_children;
                    if (tableau[y][x - 1].type === '0') {
                        tableau[y][x - 1].joueur = 1;
                        tableau[y][x - 1].dp = 1;
                    } else {
                        tableau[y][x - 1].dp = 2;
                    }
                }
            } catch (err) {
                if (pion === 'L+') {
                    num_deplacement = 5;
                } else if (pion === 'N+') {
                    num_deplacement = 7;
                } else if (pion === 'S+') {
                    num_deplacement = 13;
                } else if (pion === 'P+') {
                    num_deplacement = 9;
                } else {
                    num_deplacement = 2;
                }
            }

            // Direction 5
            try {
                if ((tableau[y][x + 1].type === '0' || tableau[y][x + 1].joueur === 2) && y <= 8 && y >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                    nom_pion = pion;
                    if (pion === 'L+') {
                        num_deplacement = 5;
                    } else if (pion === 'N+') {
                        num_deplacement = 7;
                    } else if (pion === 'S+') {
                        num_deplacement = 13;
                    } else if (pion === 'P+') {
                        num_deplacement = 9;
                    } else {
                        num_deplacement = 2;
                    }
                    num_initial = num_children;
                    if (tableau[y][x + 1].type === '0') {
                        tableau[y][x + 1].joueur = 1;
                        tableau[y][x + 1].dp = 1;
                    } else {
                        tableau[y][x + 1].dp = 2;
                    }
                }
            } catch (err) {
                if (pion === 'L+') {
                    num_deplacement = 5;
                } else if (pion === 'N+') {
                    num_deplacement = 7;
                } else if (pion === 'S+') {
                    num_deplacement = 13;
                } else if (pion === 'P+') {
                    num_deplacement = 9;
                } else {
                    num_deplacement = 2;
                }
            }

            // Direction 6
            try {
                if ((tableau[y + 1][x].type === '0' || tableau[y + 1][x].joueur === 2) && y + 1 <= 8 && y + 1 >= 0 && x <= 8 && x >= 0) {
                    nom_pion = pion;
                    if (pion === 'L+') {
                        num_deplacement = 5;
                    } else if (pion === 'N+') {
                        num_deplacement = 7;
                    } else if (pion === 'S+') {
                        num_deplacement = 13;
                    } else if (pion === 'P+') {
                        num_deplacement = 9;
                    } else {
                        num_deplacement = 2;
                    }
                    num_initial = num_children;
                    if (tableau[y + 1][x].type === '0') {
                        tableau[y + 1][x].joueur = 1;
                        tableau[y + 1][x].dp = 1;
                    } else {
                        tableau[y + 1][x].dp = 2;
                    }
                }
            } catch (err) {
                if (pion === 'L+') {
                    num_deplacement = 5;
                } else if (pion === 'N+') {
                    num_deplacement = 7;
                } else if (pion === 'S+') {
                    num_deplacement = 13;
                } else if (pion === 'P+') {
                    num_deplacement = 9;
                } else {
                    num_deplacement = 2;
                }
            }
        }

        // Si le pion est G, L+, N+, S+ ou P+ du joueur 2
        else if ((pion === 'G1' || pion === 'G2' || pion === 'L+' || pion === 'N+' || pion === 'S+' || pion === 'P+') && joueur === 2 && tourJoueur2 && tableau[y][x].dp !== 2) {
            alert("Joueur 2, SELECTION " + pion + ".");
            old_x = x;
            old_y = y;

            // Direction 1
            try {
                if ((tableau[y - 1][x - 1].type === '0' || tableau[y - 1][x - 1].joueur === 1) && y - 1 <= 8 && y - 1 >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                    nom_pion = pion;
                    if (pion === 'L+') {
                        num_deplacement = 19;
                    } else if (pion === 'N+') {
                        num_deplacement = 21;
                    } else if (pion === 'S+') {
                        num_deplacement = 27;
                    } else if (pion === 'P+') {
                        num_deplacement = 23;
                    } else {
                        num_deplacement = 16;
                    }
                    num_initial = num_children;
                    if (tableau[y - 1][x - 1].type === '0') {
                        tableau[y - 1][x - 1].joueur = 2;
                        tableau[y - 1][x - 1].dp = 1;
                    } else {
                        tableau[y - 1][x - 1].dp = 2;
                    }
                }
            } catch (err) {
                if (pion === 'L+') {
                    num_deplacement = 19;
                } else if (pion === 'N+') {
                    num_deplacement = 21;
                } else if (pion === 'S+') {
                    num_deplacement = 27;
                } else if (pion === 'P+') {
                    num_deplacement = 23;
                } else {
                    num_deplacement = 16;
                }
            }

            // Direction 2
            try {
                if ((tableau[y - 1][x].type === '0' || tableau[y - 1][x].joueur === 1) && y - 1 <= 8 && y - 1 >= 0 && x <= 8 && x >= 0) {
                    nom_pion = pion;
                    if (pion === 'L+') {
                        num_deplacement = 19;
                    } else if (pion === 'N+') {
                        num_deplacement = 21;
                    } else if (pion === 'S+') {
                        num_deplacement = 27;
                    } else if (pion === 'P+') {
                        num_deplacement = 23;
                    } else {
                        num_deplacement = 16;
                    }
                    num_initial = num_children;
                    if (tableau[y - 1][x].type === '0') {
                        tableau[y - 1][x].joueur = 2;
                        tableau[y - 1][x].dp = 1;
                    } else {
                        tableau[y - 1][x].dp = 2;
                    }
                }
            } catch (err) {
                if (pion === 'L+') {
                    num_deplacement = 19;
                } else if (pion === 'N+') {
                    num_deplacement = 21;
                } else if (pion === 'S+') {
                    num_deplacement = 27;
                } else if (pion === 'P+') {
                    num_deplacement = 23;
                } else {
                    num_deplacement = 16;
                }
            }

            // Direction 3
            try {
                if ((tableau[y - 1][x + 1].type === '0' || tableau[y - 1][x + 1].joueur === 1) && y - 1 <= 8 && y - 1 >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                    nom_pion = pion;
                    if (pion === 'L+') {
                        num_deplacement = 19;
                    } else if (pion === 'N+') {
                        num_deplacement = 21;
                    } else if (pion === 'S+') {
                        num_deplacement = 27;
                    } else if (pion === 'P+') {
                        num_deplacement = 23;
                    } else {
                        num_deplacement = 16;
                    }
                    num_initial = num_children;
                    if (tableau[y - 1][x + 1].type === '0') {
                        tableau[y - 1][x + 1].joueur = 2;
                        tableau[y - 1][x + 1].dp = 1;
                    } else {
                        tableau[y - 1][x + 1].dp = 2;
                    }
                }
            } catch (err) {
                if (pion === 'L+') {
                    num_deplacement = 19;
                } else if (pion === 'N+') {
                    num_deplacement = 21;
                } else if (pion === 'S+') {
                    num_deplacement = 27;
                } else if (pion === 'P+') {
                    num_deplacement = 23;
                } else {
                    num_deplacement = 16;
                }
            }

            // Direction 4
            try {
                if ((tableau[y][x - 1].type === '0' || tableau[y][x - 1].joueur === 1) && y <= 8 && y >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                    nom_pion = pion;
                    if (pion === 'L+') {
                        num_deplacement = 19;
                    } else if (pion === 'N+') {
                        num_deplacement = 21;
                    } else if (pion === 'S+') {
                        num_deplacement = 27;
                    } else if (pion === 'P+') {
                        num_deplacement = 23;
                    } else {
                        num_deplacement = 16;
                    }
                    num_initial = num_children;
                    if (tableau[y][x - 1].type === '0') {
                        tableau[y][x - 1].joueur = 2;
                        tableau[y][x - 1].dp = 1;
                    } else {
                        tableau[y][x - 1].dp = 2;
                    }
                }
            } catch (err) {
                if (pion === 'L+') {
                    num_deplacement = 19;
                } else if (pion === 'N+') {
                    num_deplacement = 21;
                } else if (pion === 'S+') {
                    num_deplacement = 27;
                } else if (pion === 'P+') {
                    num_deplacement = 23;
                } else {
                    num_deplacement = 16;
                }
            }

            // Direction 5
            try {
                if ((tableau[y][x + 1].type === '0' || tableau[y][x + 1].joueur === 1) && y <= 8 && y >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                    nom_pion = pion;
                    if (pion === 'L+') {
                        num_deplacement = 19;
                    } else if (pion === 'N+') {
                        num_deplacement = 21;
                    } else if (pion === 'S+') {
                        num_deplacement = 27;
                    } else if (pion === 'P+') {
                        num_deplacement = 23;
                    } else {
                        num_deplacement = 16;
                    }
                    num_initial = num_children;
                    if (tableau[y][x + 1].type === '0') {
                        tableau[y][x + 1].joueur = 2;
                        tableau[y][x + 1].dp = 1;
                    } else {
                        tableau[y][x + 1].dp = 2;
                    }
                }
            } catch (err) {
                if (pion === 'L+') {
                    num_deplacement = 19;
                } else if (pion === 'N+') {
                    num_deplacement = 21;
                } else if (pion === 'S+') {
                    num_deplacement = 27;
                } else if (pion === 'P+') {
                    num_deplacement = 23;
                } else {
                    num_deplacement = 16;
                }
            }

            // Direction 6
            try {
                if ((tableau[y + 1][x].type === '0' || tableau[y + 1][x].joueur === 1) && y + 1 <= 8 && y + 1 >= 0 && x <= 8 && x >= 0) {
                    nom_pion = pion;
                    if (pion === 'L+') {
                        num_deplacement = 19;
                    } else if (pion === 'N+') {
                        num_deplacement = 21;
                    } else if (pion === 'S+') {
                        num_deplacement = 27;
                    } else if (pion === 'P+') {
                        num_deplacement = 23;
                    } else {
                        num_deplacement = 16;
                    }
                    num_initial = num_children;
                    if (tableau[y + 1][x].type === '0') {
                        tableau[y + 1][x].joueur = 2;
                        tableau[y + 1][x].dp = 1;
                    } else {
                        tableau[y + 1][x].dp = 2;
                    }
                }
            } catch (err) {
                if (pion === 'L+') {
                    num_deplacement = 19;
                } else if (pion === 'N+') {
                    num_deplacement = 21;
                } else if (pion === 'S+') {
                    num_deplacement = 27;
                } else if (pion === 'P+') {
                    num_deplacement = 23;
                } else {
                    num_deplacement = 16;
                }
            }
        }

        // Si le pion est K du joueur 1
        else if (pion === 'K' && joueur === 1 && tourJoueur1 && tableau[y][x].dp !== 2) {
            alert("Joueur 1, SELECTION " + pion + ".");
            old_x = x;
            old_y = y;

            // Direction 1
            try {
                if ((tableau[y - 1][x - 1].type === '0' || tableau[y - 1][x - 1].joueur === 2) && y - 1 <= 8 && y - 1 >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                    nom_pion = pion;
                    nom_pion = pion;
                    num_deplacement = 3;
                    num_initial = num_children;
                    if (tableau[y - 1][x - 1].type === '0') {
                        tableau[y - 1][x - 1].joueur = 1;
                        tableau[y - 1][x - 1].dp = 1;
                    } else {
                        tableau[y - 1][x - 1].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 3;
            }

            // Direction 2
            try {
                if ((tableau[y - 1][x].type === '0' || tableau[y - 1][x].joueur === 2) && y - 1 <= 8 && y - 1 >= 0 && x <= 8 && x >= 0) {
                    nom_pion = pion;
                    nom_pion = pion;
                    num_deplacement = 3;
                    num_initial = num_children;
                    if (tableau[y - 1][x].type === '0') {
                        tableau[y - 1][x].joueur = 1;
                        tableau[y - 1][x].dp = 1;
                    } else {
                        tableau[y - 1][x].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 3;
            }

            // Direction 3
            try {
                if ((tableau[y - 1][x + 1].type === '0' || tableau[y - 1][x + 1].joueur === 2) && y - 1 <= 8 && y - 1 >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                    nom_pion = pion;
                    nom_pion = pion;
                    num_deplacement = 3;
                    num_initial = num_children;
                    if (tableau[y - 1][x + 1].type === '0') {
                        tableau[y - 1][x + 1].joueur = 1;
                        tableau[y - 1][x + 1].dp = 1;
                    } else {
                        tableau[y - 1][x + 1].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 3;
            }

            // Direction 4
            try {
                if ((tableau[y][x - 1].type === '0' || tableau[y][x - 1].joueur === 2) && y <= 8 && y >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                    nom_pion = pion;
                    nom_pion = pion;
                    num_deplacement = 3;
                    num_initial = num_children;
                    if (tableau[y][x - 1].type === '0') {
                        tableau[y][x - 1].joueur = 1;
                        tableau[y][x - 1].dp = 1;
                    } else {
                        tableau[y][x - 1].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 3;
            }

            // Direction 5
            try {
                if ((tableau[y][x + 1].type === '0' || tableau[y][x + 1].joueur === 2) && y <= 8 && y >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                    nom_pion = pion;
                    nom_pion = pion;
                    num_deplacement = 3;
                    num_initial = num_children;
                    if (tableau[y][x + 1].type === '0') {
                        tableau[y][x + 1].joueur = 1;
                        tableau[y][x + 1].dp = 1;
                    } else {
                        tableau[y][x + 1].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 3;
            }

            // Direction 6
            try {
                if ((tableau[y + 1][x - 1].type === '0' || tableau[y + 1][x - 1].joueur === 2) && y + 1 <= 8 && y + 1 >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                    nom_pion = pion;
                    nom_pion = pion;
                    num_deplacement = 3;
                    num_initial = num_children;
                    if (tableau[y + 1][x - 1].type === '0') {
                        tableau[y + 1][x - 1].joueur = 1;
                        tableau[y + 1][x - 1].dp = 1;
                    } else {
                        tableau[y + 1][x - 1].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 3;
            }

            // Direction 7
            try {
                if ((tableau[y + 1][x].type === '0' || tableau[y + 1][x].joueur === 2) && y + 1 <= 8 && y + 1 >= 0 && x <= 8 && x >= 0) {
                    nom_pion = pion;
                    nom_pion = pion;
                    num_deplacement = 3;
                    num_initial = num_children;
                    if (tableau[y + 1][x].type === '0') {
                        tableau[y + 1][x].joueur = 1;
                        tableau[y + 1][x].dp = 1;
                    } else {
                        tableau[y + 1][x].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 3;
            }

            // Direction 8
            try {
                if ((tableau[y + 1][x + 1].type === '0' || tableau[y + 1][x + 1].joueur === 2) && y + 1 <= 8 && y + 1 >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                    nom_pion = pion;
                    num_deplacement = 3;
                    num_initial = num_children;
                    if (tableau[y + 1][x + 1].type === '0') {
                        tableau[y + 1][x + 1].joueur = 1;
                        tableau[y + 1][x + 1].dp = 1;
                    } else {
                        tableau[y + 1][x + 1].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 3;
            }
        }

        // Si le pion est K du joueur 2
        else if (pion === 'K' && joueur === 2 && tourJoueur2 && tableau[y][x].dp !== 2) {
            alert("Joueur 2, SELECTION " + pion + ".");
            old_x = x;
            old_y = y;

            // Direction 1
            try {
                if ((tableau[y - 1][x - 1].type === '0' || tableau[y - 1][x - 1].joueur === 1) && y - 1 <= 8 && y - 1 >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                    nom_pion = pion;
                    num_deplacement = 17;
                    num_initial = num_children;
                    if (tableau[y - 1][x - 1].type === '0') {
                        tableau[y - 1][x - 1].joueur = 2;
                        tableau[y - 1][x - 1].dp = 1;
                    } else {
                        tableau[y - 1][x - 1].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 17;
            }

            // Direction 2
            try {
                if ((tableau[y - 1][x].type === '0' || tableau[y - 1][x].joueur === 1) && y - 1 <= 8 && y - 1 >= 0 && x <= 8 && x >= 0) {
                    nom_pion = pion;
                    num_deplacement = 17;
                    num_initial = num_children;
                    if (tableau[y - 1][x].type === '0') {
                        tableau[y - 1][x].joueur = 2;
                        tableau[y - 1][x].dp = 1;
                    } else {
                        tableau[y - 1][x].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 17;
            }

            // Direction 3
            try {
                if ((tableau[y - 1][x + 1].type === '0' || tableau[y - 1][x + 1].joueur === 1) && y - 1 <= 8 && y - 1 >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                    nom_pion = pion;
                    num_deplacement = 17;
                    num_initial = num_children;
                    if (tableau[y - 1][x + 1].type === '0') {
                        tableau[y - 1][x + 1].joueur = 2;
                        tableau[y - 1][x + 1].dp = 1;
                    } else {
                        tableau[y - 1][x + 1].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 17;
            }

            // Direction 4
            try {
                if ((tableau[y][x - 1].type === '0' || tableau[y][x - 1].joueur === 1) && y <= 8 && y >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                    nom_pion = pion;
                    num_deplacement = 17;
                    num_initial = num_children;
                    if (tableau[y][x - 1].type === '0') {
                        tableau[y][x - 1].joueur = 2;
                        tableau[y][x - 1].dp = 1;
                    } else {
                        tableau[y][x - 1].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 17;
            }

            // Direction 5
            try {
                if ((tableau[y][x + 1].type === '0' || tableau[y][x + 1].joueur === 1) && y <= 8 && y >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                    nom_pion = pion;
                    num_deplacement = 17;
                    num_initial = num_children;
                    if (tableau[y][x + 1].type === '0') {
                        tableau[y][x + 1].joueur = 2;
                        tableau[y][x + 1].dp = 1;
                    } else {
                        tableau[y][x + 1].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 17;
            }

            // Direction 6
            try {
                if ((tableau[y + 1][x - 1].type === '0' || tableau[y + 1][x - 1].joueur === 1) && y + 1 <= 8 && y + 1 >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                    nom_pion = pion;
                    num_deplacement = 17;
                    num_initial = num_children;
                    if (tableau[y + 1][x - 1].type === '0') {
                        tableau[y + 1][x - 1].joueur = 2;
                        tableau[y + 1][x - 1].dp = 1;
                    } else {
                        tableau[y + 1][x - 1].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 17;
            }

            // Direction 7
            try {
                if ((tableau[y + 1][x].type === '0' || tableau[y + 1][x].joueur === 1) && y + 1 <= 8 && y + 1 >= 0 && x <= 8 && x >= 0) {
                    nom_pion = pion;
                    num_deplacement = 17;
                    num_initial = num_children;
                    if (tableau[y + 1][x].type === '0') {
                        tableau[y + 1][x].joueur = 2;
                        tableau[y + 1][x].dp = 1;
                    } else {
                        tableau[y + 1][x].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 17;
            }

            // Direction 8
            try {
                if ((tableau[y + 1][x + 1].type === '0' || tableau[y + 1][x + 1].joueur === 1) && y + 1 <= 8 && y + 1 >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                    nom_pion = pion;
                    num_deplacement = 17;
                    num_initial = num_children;
                    if (tableau[y + 1][x + 1].type === '0') {
                        tableau[y + 1][x + 1].joueur = 2;
                        tableau[y + 1][x + 1].dp = 1;
                    } else {
                        tableau[y + 1][x + 1].dp = 2;
                    }
                }
            } catch (err) {
                num_deplacement = 17;
            }
        }

        // Si le pion est B du joueur 1
        else if ((pion === 'B' || pion === 'B+') && joueur === 1 && tourJoueur1 && tableau[y][x].dp !== 2) {
            alert("Joueur 1, SELECTION " + pion + ".");
            old_x = x;
            old_y = y;
            old2_x = x;
            old2_y = y;

            // Si le pion est B+
            if (pion === 'B+') {

                // Direction 5
                try {
                    if ((tableau[y + 1][x].type === '0' || tableau[y + 1][x].joueur === 2) && y + 1 <= 8 && y + 1 >= 0 && x <= 8 && x >= 0) {
                        nom_pion = pion;
                        num_deplacement = 1;
                        num_initial = num_children;
                        if (tableau[y + 1][x].type === '0') {
                            tableau[y + 1][x].joueur = 1;
                            tableau[y + 1][x].dp = 1;
                        } else {
                            tableau[y + 1][x].dp = 2;
                        }
                    }
                } catch (err) {
                    num_deplacement = 1;
                }

                // Direction 6
                try {
                    if ((tableau[y - 1][x].type === '0' || tableau[y - 1][x].joueur === 2) && y - 1 <= 8 && y - 1 >= 0 && x <= 8 && x >= 0) {
                        nom_pion = pion;
                        num_deplacement = 1;
                        num_initial = num_children;
                        if (tableau[y - 1][x].type === '0') {
                            tableau[y - 1][x].joueur = 1;
                            tableau[y - 1][x].dp = 1;
                        } else {
                            tableau[y - 1][x].dp = 2;
                        }
                    }
                } catch (err) {
                    num_deplacement = 1;
                }

                // Direction 7
                try {
                    if ((tableau[y][x + 1].type === '0' || tableau[y][x + 1].joueur === 2) && y <= 8 && y >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                        nom_pion = pion;
                        num_deplacement = 1;
                        num_initial = num_children;
                        if (tableau[y][x + 1].type === '0') {
                            tableau[y][x + 1].joueur = 1;
                            tableau[y][x + 1].dp = 1;
                        } else {
                            tableau[y][x + 1].dp = 2;
                        }
                    }
                } catch (err) {
                    num_deplacement = 1;
                }

                // Direction 8
                try {
                    if ((tableau[y][x - 1].type === '0' || tableau[y][x - 1].joueur === 2) && y <= 8 && y >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                        nom_pion = pion;
                        num_deplacement = 1;
                        num_initial = num_children;
                        if (tableau[y][x - 1].type === '0') {
                            tableau[y][x - 1].joueur = 1;
                            tableau[y][x - 1].dp = 1;
                        } else {
                            tableau[y][x - 1].dp = 2;
                        }
                    }
                } catch (err) {
                    num_deplacement = 1;
                }
            }

            // Toutes les cases de la direction 1
            for (let i = num_children + 8; i <= 80; i = i + 8) {
                y++;
                x--;
                try {
                    if ((tableau[y][x].type === '0' || tableau[y][x].joueur === 2) && y <= 8 && y >= 0 && x <= 8 && x >= 0) {
                        if (tableau[y][x].type === '0') {
                            tableau[y][x].joueur = 1;
                            tableau[y][x].dp = 1;
                        } else {
                            tableau[y][x].dp = 2;
                        }
                        nom_pion = pion;
                        if (pion === 'B+') {
                            num_deplacement = 1;
                        } else {
                            num_deplacement = 0;
                        }
                        num_initial = num_children;

                    } else {
                        break;
                    }
                } catch (err) {
                    if (pion === 'B+') {
                        num_deplacement = 1;
                    } else {
                        num_deplacement = 0;
                    }
                }
            }
            x = old_x;
            y = old_y;

            // Toutes les cases de la direction 2
            for (let i = num_children + 10; i <= 80; i = i + 10) {
                y++;
                x++;
                try {
                    if ((tableau[y][x].type === '0' || tableau[y][x].joueur === 2) && y <= 8 && y >= 0 && x <= 8 && x >= 0) {
                        if (tableau[y][x].type === '0') {
                            tableau[y][x].joueur = 1;
                            tableau[y][x].dp = 1;
                        } else {
                            tableau[y][x].dp = 2;
                        }
                        nom_pion = pion;
                        if (pion === 'B+') {
                            num_deplacement = 1;
                        } else {
                            num_deplacement = 0;
                        }
                        num_initial = num_children;

                    } else {
                        break;
                    }
                } catch (err) {
                    if (pion === 'B+') {
                        num_deplacement = 1;
                    } else {
                        num_deplacement = 0;
                    }
                }
            }
            x = old_x;
            y = old_y;

            // Toutes les cases de la direction 3
            for (let i = num_children - 8; i >= 0; i = i - 8) {
                y--;
                x++;
                try {
                    if ((tableau[y][x].type === '0' || tableau[y][x].joueur === 2) && y <= 8 && y >= 0 && x <= 8 && x >= 0) {
                        if (tableau[y][x].type === '0') {
                            tableau[y][x].joueur = 1;
                            tableau[y][x].dp = 1;
                        } else {
                            tableau[y][x].dp = 2;
                        }
                        nom_pion = pion;
                        if (pion === 'B+') {
                            num_deplacement = 1;
                        } else {
                            num_deplacement = 0;
                        }
                        num_initial = num_children;

                    } else {
                        break;
                    }
                } catch (err) {
                    if (pion === 'B+') {
                        num_deplacement = 1;
                    } else {
                        num_deplacement = 0;
                    }
                }
            }
            x = old_x;
            y = old_y;

            // Toutes les cases de la direction 4
            for (let i = num_children - 10; i >= 0; i = i - 10) {
                y--;
                x--;
                try {
                    if ((tableau[y][x].type === '0' || tableau[y][x].joueur === 2) && y <= 8 && y >= 0 && x <= 8 && x >= 0) {
                        if (tableau[y][x].type === '0') {
                            tableau[y][x].joueur = 1;
                            tableau[y][x].dp = 1;
                        } else {
                            tableau[y][x].dp = 2;
                        }
                        nom_pion = pion;
                        if (pion === 'B+') {
                            num_deplacement = 1;
                        } else {
                            num_deplacement = 0;
                        }
                        num_initial = num_children;

                    } else {
                        break;
                    }
                } catch (err) {
                    if (pion === 'B+') {
                        num_deplacement = 1;
                    } else {
                        num_deplacement = 0;
                    }
                }
            }
            old_x = old2_x;
            old_y = old2_y;
        }

        // Si le pion est B du joueur 2
        else if ((pion === 'B' || pion === 'B+') && joueur === 2 && tourJoueur2 && tableau[y][x].dp !== 2) {
            alert("Joueur 2, SELECTION " + pion + ".");
            old_x = x;
            old_y = y;
            old2_x = x;
            old2_y = y;

            // Si le pion est B+
            if (pion === 'B+') {

                // Direction 5
                try {
                    if ((tableau[y + 1][x].type === '0' || tableau[y + 1][x].joueur === 1) && y + 1 <= 8 && y + 1 >= 0 && x <= 8 && x >= 0) {
                        nom_pion = pion;
                        num_deplacement = 15;
                        num_initial = num_children;
                        if (tableau[y + 1][x].type === '0') {
                            tableau[y + 1][x].joueur = 2;
                            tableau[y + 1][x].dp = 1;
                        } else {
                            tableau[y + 1][x].dp = 2;
                        }
                    }
                } catch (err) {
                    num_deplacement = 15;
                }

                // Direction 6
                try {
                    if ((tableau[y - 1][x].type === '0' || tableau[y - 1][x].joueur === 1) && y - 1 <= 8 && y - 1 >= 0 && x <= 8 && x >= 0) {
                        nom_pion = pion;
                        num_deplacement = 15;
                        num_initial = num_children;
                        if (tableau[y - 1][x].type === '0') {
                            tableau[y - 1][x].joueur = 2;
                            tableau[y - 1][x].dp = 1;
                        } else {
                            tableau[y - 1][x].dp = 2;
                        }
                    }
                } catch (err) {
                    num_deplacement = 15;
                }

                // Direction 7
                try {
                    if ((tableau[y][x + 1].type === '0' || tableau[y][x + 1].joueur === 1) && y <= 8 && y >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                        nom_pion = pion;
                        num_deplacement = 15;
                        num_initial = num_children;
                        if (tableau[y][x + 1].type === '0') {
                            tableau[y][x + 1].joueur = 2;
                            tableau[y][x + 1].dp = 1;
                        } else {
                            tableau[y][x + 1].dp = 2;
                        }
                    }
                } catch (err) {
                    num_deplacement = 15;
                }

                // Direction 8
                try {
                    if ((tableau[y][x - 1].type === '0' || tableau[y][x - 1].joueur === 1) && y <= 8 && y >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                        nom_pion = pion;
                        num_deplacement = 15;
                        num_initial = num_children;
                        if (tableau[y][x - 1].type === '0') {
                            tableau[y][x - 1].joueur = 2;
                            tableau[y][x - 1].dp = 1;
                        } else {
                            tableau[y][x - 1].dp = 2;
                        }
                    }
                } catch (err) {
                    num_deplacement = 15;
                }
            }

            // Toutes les cases de la direction 1
            for (let i = num_children + 8; i <= 80; i = i + 8) {
                y++;
                x--;
                try {
                    if ((tableau[y][x].type === '0' || tableau[y][x].joueur === 1) && y <= 8 && y >= 0 && x <= 8 && x >= 0) {
                        if (tableau[y][x].type === '0') {
                            tableau[y][x].joueur = 2;
                            tableau[y][x].dp = 1;
                        } else {
                            tableau[y][x].dp = 2;
                        }
                        nom_pion = pion;
                        if (pion === 'B+') {
                            num_deplacement = 15;
                        } else {
                            num_deplacement = 14;
                        }
                        num_initial = num_children;

                    } else {
                        break;
                    }
                } catch (err) {
                    if (pion === 'B+') {
                        num_deplacement = 15;
                    } else {
                        num_deplacement = 14;
                    }
                }
            }
            x = old_x;
            y = old_y;

            // Toutes les cases de la direction 2
            for (let i = num_children + 10; i <= 80; i = i + 10) {
                y++;
                x++;
                try {
                    if ((tableau[y][x].type === '0' || tableau[y][x].joueur === 1) && y <= 8 && y >= 0 && x <= 8 && x >= 0) {
                        if (tableau[y][x].type === '0') {
                            tableau[y][x].joueur = 2;
                            tableau[y][x].dp = 1;
                        } else {
                            tableau[y][x].dp = 2;
                        }
                        nom_pion = pion;
                        if (pion === 'B+') {
                            num_deplacement = 15;
                        } else {
                            num_deplacement = 14;
                        }
                        num_initial = num_children;

                    } else {
                        break;
                    }
                } catch (err) {
                    if (pion === 'B+') {
                        num_deplacement = 15;
                    } else {
                        num_deplacement = 14;
                    }
                }
            }
            x = old_x;
            y = old_y;

            // Toutes les cases de la direction 3
            for (let i = num_children - 8; i >= 0; i = i - 8) {
                y--;
                x++;
                try {
                    if ((tableau[y][x].type === '0' || tableau[y][x].joueur === 1) && y <= 8 && y >= 0 && x <= 8 && x >= 0) {
                        if (tableau[y][x].type === '0') {
                            tableau[y][x].joueur = 2;
                            tableau[y][x].dp = 1;
                        } else {
                            tableau[y][x].dp = 2;
                        }
                        nom_pion = pion;
                        if (pion === 'B+') {
                            num_deplacement = 15;
                        } else {
                            num_deplacement = 14;
                        }
                        num_initial = num_children;

                    } else {
                        break;
                    }
                } catch (err) {
                    if (pion === 'B+') {
                        num_deplacement = 15;
                    } else {
                        num_deplacement = 14;
                    }
                }
            }
            x = old_x;
            y = old_y;

            // Toutes les cases de la direction 4
            for (let i = num_children - 10; i >= 0; i = i - 10) {
                y--;
                x--;
                try {
                    if ((tableau[y][x].type === '0' || tableau[y][x].joueur === 1) && y <= 8 && y >= 0 && x <= 8 && x >= 0) {
                        if (tableau[y][x].type === '0') {
                            tableau[y][x].joueur = 2;
                            tableau[y][x].dp = 1;
                        } else {
                            tableau[y][x].dp = 2;
                        }
                        nom_pion = pion;
                        if (pion === 'B+') {
                            num_deplacement = 15;
                        } else {
                            num_deplacement = 14;
                        }
                        num_initial = num_children;

                    } else {
                        break;
                    }
                } catch (err) {
                    if (pion === 'B+') {
                        num_deplacement = 15;
                    } else {
                        num_deplacement = 14;
                    }
                }
            }
            old_x = old2_x;
            old_y = old2_y;
        }

        // Si le pion est R du joueur 1
        else if ((pion === 'R' || pion === 'R+') && joueur === 1 && tourJoueur1 && tableau[y][x].dp !== 2) {
            alert("Joueur 1, SELECTION " + pion + ".");
            old_x = x;
            old_y = y;
            old2_x = x;
            old2_y = y;

            // Si le pion est R+
            if (pion === 'R+') {

                // Direction 5
                try {
                    if ((tableau[y + 1][x + 1].type === '0' || tableau[y + 1][x + 1].joueur === 2) && y + 1 <= 8 && y + 1 >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                        nom_pion = pion;
                        num_deplacement = 11;
                        num_initial = num_children;
                        if (tableau[y + 1][x + 1].type === '0') {
                            tableau[y + 1][x + 1].joueur = 1;
                            tableau[y + 1][x + 1].dp = 1;
                        } else {
                            tableau[y + 1][x + 1].dp = 2;
                        }
                    }
                } catch (err) {
                    num_deplacement = 11;
                }

                // Direction 6
                try {
                    if ((tableau[y - 1][x + 1].type === '0' || tableau[y - 1][x + 1].joueur === 2) && y - 1 <= 8 && y - 1 >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                        nom_pion = pion;
                        num_deplacement = 11;
                        num_initial = num_children;
                        if (tableau[y - 1][x + 1].type === '0') {
                            tableau[y - 1][x + 1].joueur = 1;
                            tableau[y - 1][x + 1].dp = 1;
                        } else {
                            tableau[y - 1][x + 1].dp = 2;
                        }
                    }
                } catch (err) {
                    num_deplacement = 11;
                }

                // Direction 7
                try {
                    if ((tableau[y + 1][x - 1].type === '0' || tableau[y + 1][x - 1].joueur === 2) && y + 1 <= 8 && y + 1 >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                        nom_pion = pion;
                        num_deplacement = 11;
                        num_initial = num_children;
                        if (tableau[y + 1][x - 1].type === '0') {
                            tableau[y + 1][x - 1].joueur = 1;
                            tableau[y + 1][x - 1].dp = 1;
                        } else {
                            tableau[y + 1][x - 1].dp = 2;
                        }
                    }
                } catch (err) {
                    num_deplacement = 11;
                }

                // Direction 8
                try {
                    if ((tableau[y - 1][x - 1].type === '0' || tableau[y - 1][x - 1].joueur === 2) && y - 1 <= 8 && y - 1 >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                        nom_pion = pion;
                        num_deplacement = 11;
                        num_initial = num_children;
                        if (tableau[y - 1][x - 1].type === '0') {
                            tableau[y - 1][x - 1].joueur = 1;
                            tableau[y - 1][x - 1].dp = 1;
                        } else {
                            tableau[y - 1][x - 1].dp = 2;
                        }
                    }
                } catch (err) {
                    num_deplacement = 11;
                }
            }

            // Toutes les cases de la direction 1
            for (let i = num_children + 9; i <= 80; i = i + 9) {
                y--;
                try {
                    if ((tableau[y][x].type === '0' || tableau[y][x].joueur === 2) && y <= 8 && y >= 0 && x <= 8 && x >= 0) {
                        if (tableau[y][x].type === '0') {
                            tableau[y][x].joueur = 1;
                            tableau[y][x].dp = 1;
                        } else {
                            tableau[y][x].dp = 2;
                        }
                        nom_pion = pion;
                        if (pion === 'R+') {
                            num_deplacement = 11;
                        } else {
                            num_deplacement = 10;
                        }
                        num_initial = num_children;

                    } else {
                        break;
                    }
                } catch (err) {
                    if (pion === 'R+') {
                        num_deplacement = 11;
                    } else {
                        num_deplacement = 10;
                    }
                }
            }
            x = old_x;
            y = old_y;

            // Toutes les cases de la direction 2
            for (let i = num_children - 9; i >= 0; i = i - 9) {
                y++;
                try {
                    if ((tableau[y][x].type === '0' || tableau[y][x].joueur === 2) && y <= 8 && y >= 0 && x <= 8 && x >= 0) {
                        if (tableau[y][x].type === '0') {
                            tableau[y][x].joueur = 1;
                            tableau[y][x].dp = 1;
                        } else {
                            tableau[y][x].dp = 2;
                        }
                        nom_pion = pion;
                        if (pion === 'R+') {
                            num_deplacement = 11;
                        } else {
                            num_deplacement = 10;
                        }
                        num_initial = num_children;
                    } else {
                        break;
                    }
                } catch (err) {
                    if (pion === 'R+') {
                        num_deplacement = 11;
                    } else {
                        num_deplacement = 10;
                    }
                }
            }
            x = old_x;
            y = old_y;

            // Toutes les cases de la direction 3
            for (let i = num_children - 1; i >= y * 9; i = i - 1) {
                x--;
                try {
                    if ((tableau[y][x].type === '0' || tableau[y][x].joueur === 2) && y <= 8 && y >= 0 && x <= 8 && x >= 0) {
                        if (tableau[y][x].type === '0') {
                            tableau[y][x].joueur = 1;
                            tableau[y][x].dp = 1;
                        } else {
                            tableau[y][x].dp = 2;
                        }
                        nom_pion = pion;
                        if (pion === 'R+') {
                            num_deplacement = 11;
                        } else {
                            num_deplacement = 10;
                        }
                        num_initial = num_children;

                    } else {
                        break;
                    }
                } catch (err) {
                    if (pion === 'R+') {
                        num_deplacement = 11;
                    } else {
                        num_deplacement = 10;
                    }
                }
            }
            x = old_x;
            y = old_y;

            // Toutes les cases de la direction 4
            for (let i = num_children + 1; i < (y + 1) * 9; i = i + 1) {
                x++;
                try {
                    if ((tableau[y][x].type === '0' || tableau[y][x].joueur === 2) && y <= 8 && y >= 0 && x <= 8 && x >= 0) {
                        if (tableau[y][x].type === '0') {
                            tableau[y][x].joueur = 1;
                            tableau[y][x].dp = 1;
                        } else {
                            tableau[y][x].dp = 2;
                        }
                        nom_pion = pion;
                        if (pion === 'R+') {
                            num_deplacement = 11;
                        } else {
                            num_deplacement = 10;
                        }
                        num_initial = num_children;
                    } else {
                        break;
                    }
                } catch (err) {
                    if (pion === 'R+') {
                        num_deplacement = 11;
                    } else {
                        num_deplacement = 10;
                    }
                }
            }
            old_x = old2_x;
            old_y = old2_y;
        }

        // Si le pion est R du joueur 2
        else if ((pion === 'R' || pion === 'R+') && joueur === 2 && tourJoueur2 && tableau[y][x].dp !== 2) {
            alert("Joueur 2, SELECTION " + pion + ".");
            old_x = x;
            old_y = y;
            old2_x = x;
            old2_y = y;

            // Si le pion est R+
            if (pion === 'R+') {

                // Direction 5
                try {
                    if ((tableau[y + 1][x + 1].type === '0' || tableau[y + 1][x + 1].joueur === 1) && y + 1 <= 8 && y + 1 >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                        nom_pion = pion;
                        num_deplacement = 25;
                        num_initial = num_children;
                        if (tableau[y + 1][x + 1].type === '0') {
                            tableau[y + 1][x + 1].joueur = 2;
                            tableau[y + 1][x + 1].dp = 1;
                        } else {
                            tableau[y + 1][x + 1].dp = 2;
                        }
                    }
                } catch (err) {
                    num_deplacement = 25;
                }

                // Direction 6
                try {
                    if ((tableau[y - 1][x + 1].type === '0' || tableau[y - 1][x + 1].joueur === 1) && y - 1 <= 8 && y - 1 >= 0 && x + 1 <= 8 && x + 1 >= 0) {
                        nom_pion = pion;
                        num_deplacement = 25;
                        num_initial = num_children;
                        if (tableau[y - 1][x + 1].type === '0') {
                            tableau[y - 1][x + 1].joueur = 2;
                            tableau[y - 1][x + 1].dp = 1;
                        } else {
                            tableau[y - 1][x + 1].dp = 2;
                        }
                    }
                } catch (err) {
                    num_deplacement = 25;
                }

                // Direction 7
                try {
                    if ((tableau[y + 1][x - 1].type === '0' || tableau[y + 1][x - 1].joueur === 1) && y + 1 <= 8 && y + 1 >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                        nom_pion = pion;
                        num_deplacement = 25;
                        num_initial = num_children;
                        if (tableau[y + 1][x - 1].type === '0') {
                            tableau[y + 1][x - 1].joueur = 2;
                            tableau[y + 1][x - 1].dp = 1;
                        } else {
                            tableau[y + 1][x - 1].dp = 2;
                        }
                    }
                } catch (err) {
                    num_deplacement = 25;
                }

                // Direction 8
                try {
                    if ((tableau[y - 1][x - 1].type === '0' || tableau[y - 1][x - 1].joueur === 1) && y - 1 <= 8 && y - 1 >= 0 && x - 1 <= 8 && x - 1 >= 0) {
                        nom_pion = pion;
                        num_deplacement = 25;
                        num_initial = num_children;
                        if (tableau[y - 1][x - 1].type === '0') {
                            tableau[y - 1][x - 1].joueur = 2;
                            tableau[y - 1][x - 1].dp = 1;
                        } else {
                            tableau[y - 1][x - 1].dp = 2;
                        }
                    }
                } catch (err) {
                    num_deplacement = 25;
                }
            }

            // Toutes les cases de la direction 1
            for (let i = num_children + 9; i <= 80; i = i + 9) {
                y++;
                try {
                    if ((tableau[y][x].type === '0' || tableau[y][x].joueur === 1) && y <= 8 && y >= 0 && x <= 8 && x >= 0) {
                        if (tableau[y][x].type === '0') {
                            tableau[y][x].joueur = 2;
                            tableau[y][x].dp = 1;
                        } else {
                            tableau[y][x].dp = 2;
                        }
                        nom_pion = pion;
                        if (pion === 'R+') {
                            num_deplacement = 25;
                        } else {
                            num_deplacement = 24;
                        }
                        num_initial = num_children;
                    } else {
                        break;
                    }
                } catch (err) {
                    if (pion === 'R+') {
                        num_deplacement = 25;
                    } else {
                        num_deplacement = 24;
                    }
                }
            }
            x = old_x;
            y = old_y;

            // Toutes les cases de la direction 2
            for (let i = num_children - 9; i >= 0; i = i - 9) {
                y--;
                try {
                    if ((tableau[y][x].type === '0' || tableau[y][x].joueur === 1) && y <= 8 && y >= 0 && x <= 8 && x >= 0) {
                        if (tableau[y][x].type === '0') {
                            tableau[y][x].joueur = 2;
                            tableau[y][x].dp = 1;
                        } else {
                            tableau[y][x].dp = 2;
                        }
                        nom_pion = pion;
                        if (pion === 'R+') {
                            num_deplacement = 25;
                        } else {
                            num_deplacement = 24;
                        }
                        num_initial = num_children;
                    } else {
                        break;
                    }
                } catch (err) {
                    if (pion === 'R+') {
                        num_deplacement = 25;
                    } else {
                        num_deplacement = 24;
                    }
                }
            }
            x = old_x;
            y = old_y;

            // Toutes les cases de la direction 3
            for (let i = num_children - 1; i >= y * 9; i = i - 1) {
                x--;
                try {
                    if ((tableau[y][x].type === '0' || tableau[y][x].joueur === 1) && y <= 8 && y >= 0 && x <= 8 && x >= 0) {
                        if (tableau[y][x].type === '0') {
                            tableau[y][x].joueur = 2;
                            tableau[y][x].dp = 1;
                        } else {
                            tableau[y][x].dp = 2;
                        }
                        nom_pion = pion;
                        if (pion === 'R+') {
                            num_deplacement = 25;
                        } else {
                            num_deplacement = 24;
                        }
                        num_initial = num_children;
                    } else {
                        break;
                    }
                } catch (err) {
                    if (pion === 'R+') {
                        num_deplacement = 25;
                    } else {
                        num_deplacement = 24;
                    }
                }
            }
            x = old_x;
            y = old_y;

            // Toutes les cases de la direction 4
            for (let i = num_children + 1; i < (y + 1) * 9; i = i + 1) {
                x++;
                try {
                    if ((tableau[y][x].type === '0' || tableau[y][x].joueur === 1) && y <= 8 && y >= 0 && x <= 8 && x >= 0) {
                        if (tableau[y][x].type === '0') {
                            tableau[y][x].joueur = 2;
                            tableau[y][x].dp = 1;
                        } else {
                            tableau[y][x].dp = 2;
                        }
                        nom_pion = pion;
                        if (pion === 'R+') {
                            num_deplacement = 25;
                        } else {
                            num_deplacement = 24;
                        }
                        num_initial = num_children;
                    } else {
                        break;
                    }
                } catch (err) {
                    if (pion === 'R+') {
                        num_deplacement = 25;
                    } else {
                        num_deplacement = 24;
                    }
                }
            }
            old_x = old2_x;
            old_y = old2_y;
        }

        // Si le pion est P du joueur 1
        else if ((pion === 'P1' || pion === 'P2' || pion === 'P3' || pion === 'P4' || pion === 'P5' ||
            pion === 'P6' || pion === 'P7' || pion === 'P8' || pion === 'P9') && joueur === 1 && tourJoueur1 && tableau[y][x].dp !== 2) {
            alert("Joueur 1, SELECTION " + pion + ".");
            old_x = x;
            old_y = y;

            // Direction 1
            try {
                if ((tableau[y - 1][x].type === '0' || tableau[y - 1][x].joueur === 2) && y - 1 <= 8 && y - 1 >= 0 && x <= 8 && x >= 0) {
                    if (tableau[y - 1][x].type === '0') {
                        tableau[y - 1][x].joueur = 1;
                        tableau[y - 1][x].dp = 1;
                    } else {
                        tableau[y - 1][x].dp = 2;
                    }
                    num_initial = num_children;
                    nom_pion = pion;
                    num_deplacement = 8;
                }
            } catch (err) {
                num_deplacement = 8;
            }
        }

        // Si le pion est P du joueur 2
        else if ((pion === 'P1' || pion === 'P2' || pion === 'P3' || pion === 'P4' || pion === 'P5' ||
            pion === 'P6' || pion === 'P7' || pion === 'P8' || pion === 'P9') && joueur === 2 && tourJoueur2 && tableau[y][x].dp !== 2) {
            alert("Joueur 2, SELECTION " + pion + ".");
            old_x = x;
            old_y = y;

            // Direction 1
            try {
                if ((tableau[y + 1][x].type === '0' || tableau[y + 1][x].joueur === 1) && y + 1 <= 8 && y + 1 >= 0 && x <= 8 && x >= 0) {
                    if (tableau[y + 1][x].type === '0') {
                        tableau[y + 1][x].joueur = 2;
                        tableau[y + 1][x].dp = 1;
                    } else {
                        tableau[y + 1][x].dp = 2;
                    }
                    num_initial = num_children;
                    nom_pion = pion;
                    num_deplacement = 22;
                }
            } catch (err) {
                num_deplacement = 22;
            }
        }

        // Si une case de deplacement ou de remplacement est selectionne
        else if (tableau[y][x].dp === 1 || tableau[y][x].dp === 2) {

            // Enlever le pion est le noter lorsqu'un remplacement se produit
            if (tableau[y][x].dp === 2) {
                removeImage(uiTableau.children[num_children], num_children);
                pion_devore = tableau[y][x].type;
                pion_devore_joueur = tableau[y][x].joueur;
            }

            // Enlever et deplacer l'image lors d'un deplacement
            removeImage(uiTableau.children[num_initial], num_initial);
            addImage(uiTableau.children[num_children], num_children, num_deplacement);
            tableau[y][x] = {type: nom_pion, joueur: id_joueur, dp: 0};
            tableau[old_y][old_x] = {type: '0', joueur: 0, dp: 0};

            // Verifier si la promotion du pion est possible
            PromotionPion(x, y, nom_pion, id_joueur);

            // Regler tous les deplacements a 0 sur la grille du jeu
            for (let i = 0; i < tableau.length; i++) {
                for (let j = 0; j < tableau[0].length; j++) {
                    tableau[i][j].dp = 0;
                }
            }

            // Termine la partie si le roi est devore
            if (pion_devore === 'K' && pion_devore_joueur === 2) {
                alert("Victoire du Joueur 1!!!");
                partieTerminee = true;
            } else if (pion_devore === 'K' && pion_devore_joueur === 1) {
                alert("Victoire du Joueur 2!!!");
                partieTerminee = true;
            }

            // Reinitialise les letiables pour eviter les erreurs
            old_y = null;
            old_x = null;
            old2_y = null;
            old2_x = null;
            pion_devore = null;
            pion_devore_joueur = null;

            // Changer la letiable du tour
            if (tourJoueur1) {
                tourJoueur1 = false;
                tourJoueur2 = true;
            } else if (tourJoueur2) {
                tourJoueur1 = true;
                tourJoueur2 = false;
            }

            // Changer l'identifiant du joueur chaque tour
            if (tourJoueur1) {
                id_joueur = 1;
            } else if (tourJoueur2) {
                id_joueur = 2;
            }
        }
    }

    // Ajout d'images au debut la partie pour tous les pions
    addImage(uiTableau.children[0], 0, 18);
    addImage(uiTableau.children[1], 1, 20);
    addImage(uiTableau.children[2], 2, 26);
    addImage(uiTableau.children[3], 3, 16);
    addImage(uiTableau.children[4], 4, 17);
    addImage(uiTableau.children[5], 5, 16);
    addImage(uiTableau.children[6], 6, 26);
    addImage(uiTableau.children[7], 7, 20);
    addImage(uiTableau.children[8], 8, 18);
    addImage(uiTableau.children[10], 10, 24);
    addImage(uiTableau.children[16], 16, 14);
    addImage(uiTableau.children[18], 18, 22);
    addImage(uiTableau.children[19], 19, 22);
    addImage(uiTableau.children[20], 20, 22);
    addImage(uiTableau.children[21], 21, 22);
    addImage(uiTableau.children[22], 22, 22);
    addImage(uiTableau.children[23], 23, 22);
    addImage(uiTableau.children[24], 24, 22);
    addImage(uiTableau.children[25], 25, 22);
    addImage(uiTableau.children[26], 26, 22);
    addImage(uiTableau.children[54], 54, 8);
    addImage(uiTableau.children[55], 55, 8);
    addImage(uiTableau.children[56], 56, 8);
    addImage(uiTableau.children[57], 57, 8);
    addImage(uiTableau.children[58], 58, 8);
    addImage(uiTableau.children[59], 59, 8);
    addImage(uiTableau.children[60], 60, 8);
    addImage(uiTableau.children[61], 61, 8);
    addImage(uiTableau.children[62], 62, 8);
    addImage(uiTableau.children[64], 64, 0);
    addImage(uiTableau.children[70], 70, 10);
    addImage(uiTableau.children[72], 72, 4);
    addImage(uiTableau.children[73], 73, 6);
    addImage(uiTableau.children[74], 74, 12);
    addImage(uiTableau.children[75], 75, 2);
    addImage(uiTableau.children[76], 76, 3);
    addImage(uiTableau.children[77], 77, 2);
    addImage(uiTableau.children[78], 78, 12);
    addImage(uiTableau.children[79], 79, 6);
    addImage(uiTableau.children[80], 80, 4);

    // Configuration de toutes les cases selon l'interface graphique
    let case_0_0 = uiTableau.children[0];
    let case_0_1 = uiTableau.children[1];
    let case_0_2 = uiTableau.children[2];
    let case_0_3 = uiTableau.children[3];
    let case_0_4 = uiTableau.children[4];
    let case_0_5 = uiTableau.children[5];
    let case_0_6 = uiTableau.children[6];
    let case_0_7 = uiTableau.children[7];
    let case_0_8 = uiTableau.children[8];

    let case_1_0 = uiTableau.children[9];
    let case_1_1 = uiTableau.children[10];
    let case_1_2 = uiTableau.children[11];
    let case_1_3 = uiTableau.children[12];
    let case_1_4 = uiTableau.children[13];
    let case_1_5 = uiTableau.children[14];
    let case_1_6 = uiTableau.children[15];
    let case_1_7 = uiTableau.children[16];
    let case_1_8 = uiTableau.children[17];

    let case_2_0 = uiTableau.children[18];
    let case_2_1 = uiTableau.children[19];
    let case_2_2 = uiTableau.children[20];
    let case_2_3 = uiTableau.children[21];
    let case_2_4 = uiTableau.children[22];
    let case_2_5 = uiTableau.children[23];
    let case_2_6 = uiTableau.children[24];
    let case_2_7 = uiTableau.children[25];
    let case_2_8 = uiTableau.children[26];

    let case_3_0 = uiTableau.children[27];
    let case_3_1 = uiTableau.children[28];
    let case_3_2 = uiTableau.children[29];
    let case_3_3 = uiTableau.children[30];
    let case_3_4 = uiTableau.children[31];
    let case_3_5 = uiTableau.children[32];
    let case_3_6 = uiTableau.children[33];
    let case_3_7 = uiTableau.children[34];
    let case_3_8 = uiTableau.children[35];

    let case_4_0 = uiTableau.children[36];
    let case_4_1 = uiTableau.children[37];
    let case_4_2 = uiTableau.children[38];
    let case_4_3 = uiTableau.children[39];
    let case_4_4 = uiTableau.children[40];
    let case_4_5 = uiTableau.children[41];
    let case_4_6 = uiTableau.children[42];
    let case_4_7 = uiTableau.children[43];
    let case_4_8 = uiTableau.children[44];

    let case_5_0 = uiTableau.children[45];
    let case_5_1 = uiTableau.children[46];
    let case_5_2 = uiTableau.children[47];
    let case_5_3 = uiTableau.children[48];
    let case_5_4 = uiTableau.children[49];
    let case_5_5 = uiTableau.children[50];
    let case_5_6 = uiTableau.children[51];
    let case_5_7 = uiTableau.children[52];
    let case_5_8 = uiTableau.children[53];

    let case_6_0 = uiTableau.children[54];
    let case_6_1 = uiTableau.children[55];
    let case_6_2 = uiTableau.children[56];
    let case_6_3 = uiTableau.children[57];
    let case_6_4 = uiTableau.children[58];
    let case_6_5 = uiTableau.children[59];
    let case_6_6 = uiTableau.children[60];
    let case_6_7 = uiTableau.children[61];
    let case_6_8 = uiTableau.children[62];

    let case_7_0 = uiTableau.children[63];
    let case_7_1 = uiTableau.children[64];
    let case_7_2 = uiTableau.children[65];
    let case_7_3 = uiTableau.children[66];
    let case_7_4 = uiTableau.children[67];
    let case_7_5 = uiTableau.children[68];
    let case_7_6 = uiTableau.children[69];
    let case_7_7 = uiTableau.children[70];
    let case_7_8 = uiTableau.children[71];

    let case_8_0 = uiTableau.children[72];
    let case_8_1 = uiTableau.children[73];
    let case_8_2 = uiTableau.children[74];
    let case_8_3 = uiTableau.children[75];
    let case_8_4 = uiTableau.children[76];
    let case_8_5 = uiTableau.children[77];
    let case_8_6 = uiTableau.children[78];
    let case_8_7 = uiTableau.children[79];
    let case_8_8 = uiTableau.children[80];


    // Configuration du deplacement des pions selon la case selectionne (case_y_x)
    case_0_0.onclick = () => {
        position_x = 0;
        position_y = 0;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_0_1.onclick = () => {
        position_x = 1;
        position_y = 0;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_0_2.onclick = () => {
        position_x = 2;
        position_y = 0;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_0_3.onclick = () => {
        position_x = 3;
        position_y = 0;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_0_4.onclick = () => {
        position_x = 4;
        position_y = 0;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_0_5.onclick = () => {
        position_x = 5;
        position_y = 0;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_0_6.onclick = () => {
        position_x = 6;
        position_y = 0;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_0_7.onclick = () => {
        position_x = 7;
        position_y = 0;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_0_8.onclick = () => {
        position_x = 8;
        position_y = 0;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_1_0.onclick = () => {
        position_x = 0;
        position_y = 1;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_1_1.onclick = () => {
        position_x = 1;
        position_y = 1;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_1_2.onclick = () => {
        position_x = 2;
        position_y = 1;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_1_3.onclick = () => {
        position_x = 3;
        position_y = 1;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_1_4.onclick = () => {
        position_x = 4;
        position_y = 1;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_1_5.onclick = () => {
        position_x = 5;
        position_y = 1;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_1_6.onclick = () => {
        position_x = 6;
        position_y = 1;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_1_7.onclick = () => {
        position_x = 7;
        position_y = 1;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_1_8.onclick = () => {
        position_x = 8;
        position_y = 1;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_2_0.onclick = () => {
        position_x = 0;
        position_y = 2;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_2_1.onclick = () => {
        position_x = 1;
        position_y = 2;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_2_2.onclick = () => {
        position_x = 2;
        position_y = 2;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_2_3.onclick = () => {
        position_x = 3;
        position_y = 2;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_2_4.onclick = () => {
        position_x = 4;
        position_y = 2;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_2_5.onclick = () => {
        position_x = 5;
        position_y = 2;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_2_6.onclick = () => {
        position_x = 6;
        position_y = 2;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_2_7.onclick = () => {
        position_x = 7;
        position_y = 2;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_2_8.onclick = () => {
        position_x = 8;
        position_y = 2;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_3_0.onclick = () => {
        position_x = 0;
        position_y = 3;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_3_1.onclick = () => {
        position_x = 1;
        position_y = 3;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_3_2.onclick = () => {
        position_x = 2;
        position_y = 3;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_3_3.onclick = () => {
        position_x = 3;
        position_y = 3;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_3_4.onclick = () => {
        position_x = 4;
        position_y = 3;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_3_5.onclick = () => {
        position_x = 5;
        position_y = 3;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_3_6.onclick = () => {
        position_x = 6;
        position_y = 3;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_3_7.onclick = () => {
        position_x = 7;
        position_y = 3;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_3_8.onclick = () => {
        position_x = 8;
        position_y = 3;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_4_0.onclick = () => {
        position_x = 0;
        position_y = 4;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_4_1.onclick = () => {
        position_x = 1;
        position_y = 4;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_4_2.onclick = () => {
        position_x = 2;
        position_y = 4;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_4_3.onclick = () => {
        position_x = 3;
        position_y = 4;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_4_4.onclick = () => {
        position_x = 4;
        position_y = 4;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_4_5.onclick = () => {
        position_x = 5;
        position_y = 4;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_4_6.onclick = () => {
        position_x = 6;
        position_y = 4;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_4_7.onclick = () => {
        position_x = 7;
        position_y = 4;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_4_8.onclick = () => {
        position_x = 8;
        position_y = 4;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_5_0.onclick = () => {
        position_x = 0;
        position_y = 5;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_5_1.onclick = () => {
        position_x = 1;
        position_y = 5;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_5_2.onclick = () => {
        position_x = 2;
        position_y = 5;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_5_3.onclick = () => {
        position_x = 3;
        position_y = 5;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_5_4.onclick = () => {
        position_x = 4;
        position_y = 5;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_5_5.onclick = () => {
        position_x = 5;
        position_y = 5;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_5_6.onclick = () => {
        position_x = 6;
        position_y = 5;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_5_7.onclick = () => {
        position_x = 7;
        position_y = 5;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_5_8.onclick = () => {
        position_x = 8;
        position_y = 5;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_6_0.onclick = () => {
        position_x = 0;
        position_y = 6;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_6_1.onclick = () => {
        position_x = 1;
        position_y = 6;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_6_2.onclick = () => {
        position_x = 2;
        position_y = 6;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_6_3.onclick = () => {
        position_x = 3;
        position_y = 6;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_6_4.onclick = () => {
        position_x = 4;
        position_y = 6;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_6_5.onclick = () => {
        position_x = 5;
        position_y = 6;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_6_6.onclick = () => {
        position_x = 6;
        position_y = 6;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_6_7.onclick = () => {
        position_x = 7;
        position_y = 6;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_6_8.onclick = () => {
        position_x = 8;
        position_y = 6;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_7_0.onclick = () => {
        position_x = 0;
        position_y = 7;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {

            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_7_1.onclick = () => {
        position_x = 1;
        position_y = 7;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_7_2.onclick = () => {
        position_x = 2;
        position_y = 7;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_7_3.onclick = () => {
        position_x = 3;
        position_y = 7;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_7_4.onclick = () => {
        position_x = 4;
        position_y = 7;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_7_5.onclick = () => {
        position_x = 5;
        position_y = 7;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_7_6.onclick = () => {
        position_x = 6;
        position_y = 7;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_7_7.onclick = () => {
        position_x = 7;
        position_y = 7;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_7_8.onclick = () => {
        position_x = 8;
        position_y = 7;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_8_0.onclick = () => {
        position_x = 0;
        position_y = 8;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_8_1.onclick = () => {
        position_x = 1;
        position_y = 8;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_8_2.onclick = () => {
        position_x = 2;
        position_y = 8;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_8_3.onclick = () => {
        position_x = 3;
        position_y = 8;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_8_4.onclick = () => {
        position_x = 4;
        position_y = 8;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_8_5.onclick = () => {
        position_x = 5;
        position_y = 8;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_8_6.onclick = () => {
        position_x = 6;
        position_y = 8;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_8_7.onclick = () => {
        position_x = 7;
        position_y = 8;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };

    case_8_8.onclick = () => {
        position_x = 8;
        position_y = 8;
        num_pion = tableau[position_y][position_x].type;
        num_joueur = tableau[position_y][position_x].joueur;
        num_dp = tableau[position_y][position_x].dp;
        if ((num_dp === 2 || num_joueur === id_joueur) && !partieTerminee) {
            DetectionDeplacement(position_x, position_y, num_pion, num_joueur);
        }
    };
}

// Programme
shogi();